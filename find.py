import os
import csv
from CONFIG import *

def find_image_paths():
    datos = list(os.walk(FIND_FILES_IN))
    
    datos = datos[0]
    ruta = datos[0]
    all_images = []
    for name in datos[2]:
        all_images.append(os.path.join(ruta, name))
    return tuple(all_images)


def build_dict_for_tagged():
    dict = {}
    with open(SAVE_TAGS_IN) as archivo:
        archivo_csv = csv.reader(archivo)
        for linea in archivo_csv:
            tag = linea[0]
            paths = linea[1].split()
            dict[tag] = [path for path in paths if path.endswith(EXTENSIONES_BUSCADAS)]
    return dict


