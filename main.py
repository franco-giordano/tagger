import os
import android
import random
import csv
from time import *
from threading import Thread
import sys
  	
  	
"""**************
* VERSION 0.2 *
**************"""



CREDITOS = """IMAGE TAGGER, FRANCO GIORDANO 2017. VERSION 0.2"""
CONFIG_DEFAULT = """COMANDOS_FIND = ("f", "find")
COMANDOS_ASSIGN = ("a", "assign", "t", "taggear")

COMANDOS_SKIP = ("",".s")

SAVE_TAGS_IN = "/sdcard/tags.txt"
FIND_FILES_IN = "/sdcard/Pictures"

EXTENSIONES_BUSCADAS = (".jpg", ".jpeg", ".png")
"""
MODOS = ("and:","or:","not:")
DROID = android.Android()

def do_config_file():
    RUTA_CODIGO = os.path.join(sys.path[0],"CONFIG.py")
    
    try:
        open(RUTA_CODIGO)
    except IOError:
        print("Archivo CONFIG no hallado, creando...")
        with open(RUTA_CODIGO,"w") as archivo:
            archivo.write(CONFIG_DEFAULT)


do_config_file()

from CONFIG import *
from find import *
INDICE_INVERTIDO = build_dict_for_tagged()
    
def invertir_diccionario(diccionario):
    '''Dado un diccionario, devuelve un nuevo diccionario donde cada clave sera un valor del anterior, y sus valores seran claves del anterior.
    Ej: {"animal": ["perro"], "persona": ["alumno","bombero"]}  ----->  {"perro": ["animal"], "alumno": ["persona"], "bombero": ["persona"]}'''
    diccionario_invertido = {}
    for clave, lista in diccionario.items():
        for elemento in lista:
            diccionario_invertido[elemento] = diccionario_invertido.get(elemento, []) + [clave]
    return diccionario_invertido

INDICE = invertir_diccionario(INDICE_INVERTIDO)

#fin constantes


def recibir_comandos():
    """Utilizada en main(). Imprime un prompt y recibe un input. Devuelve tres elementos: un elemento de MODOS (correspondiente al elegido), terminos elegidos (palabras), e input original (sin MAYUS/minus)."""
    input_raw = input("F> ").lower()
    palabras = input_raw.split()
    modo_elegido = "or:"

    if input_raw.startswith(MODOS):
        modo_elegido = palabras[0]
        palabras.pop(0)
    
    return modo_elegido,palabras,input_raw

def remover_repetidos(lista):
    '''Dada una lista, devuelve una nueva desordenada sin elementos repetidos.'''
    return list(set(lista))

def obtener_elem_compartidos_entre(lista1, lista2):
    """Dadas dos listas, encuentra los elementos compartidos en ambas y los devuelve como una nueva lista."""
    return [elemento1 for elemento1 in lista1 if elemento1 in lista2]


def modo_find():
    '''.'''
    while True:
        modo_busqueda,terminos_a_buscar,input_raw = recibir_comandos()
        
        if input_raw == ".":
            """decidir_comando_especial(input_raw)
            continue"""
            return

        rutas_coincidentes = []
        un_solo_term_a_buscar = len(terminos_a_buscar) == 1

        if modo_busqueda == "or:":
            for termino in terminos_a_buscar:
                rutas_coincidentes += INDICE_INVERTIDO.get(termino,[])
        

        elif modo_busqueda == "and:":
            for i,termino in enumerate(terminos_a_buscar):
                if termino not in INDICE_INVERTIDO:
                    rutas_coincidentes = []
                    break
                if i == 0:
                    rutas_coincidentes = INDICE_INVERTIDO[termino]
                rutas_coincidentes = obtener_elem_compartidos_entre(rutas_coincidentes,INDICE_INVERTIDO[termino])

        elif modo_busqueda == "not:":
            if not un_solo_term_a_buscar:
                print("La busqueda 'not' recibe un solo termino! Intente nuevamente\n")
                continue
            rutas_no_validas = INDICE_INVERTIDO.get(terminos_a_buscar[0], [])
            for termino,rutas in INDICE_INVERTIDO.items():
                rutas_coincidentes += rutas
            rutas_coincidentes = [ruta for ruta in rutas_coincidentes if ruta not in rutas_no_validas]

        if rutas_coincidentes == []:
            print("No hay coincidencias\n")
            continue

        rutas_coincidentes = remover_repetidos(rutas_coincidentes)
        total = len(rutas_coincidentes)
        print("{} ruta(s) encontrada(s)!\n".format(total))
        for i,r_coinc in enumerate(rutas_coincidentes):
            DROID.makeToast("{}/{}".format(i+1,total))
            print("{}: {}".format(i+1, " ".join(INDICE[r_coinc])))
            DROID.view('file://{}'.format(r_coinc), 'image/*')
            """sleep = Thread(sleep(3)).start()
            inp = Thread(input("lalal")).start()
            """
            if input():
                break
        





def modo_assign():
    all_images = find_image_paths()
    tagged = []
    for lista in INDICE_INVERTIDO.values():
        tagged.extend(lista)
    
    #tagged = [path for lista in INDICE_INVERTIDO.values() for path in lista]
    untagged = list(set(all_images)-set(tagged))
    """for path in all_images:
        if path not in tagged:
            untagged.append(path)"""
    print("Sin taggear: {} imagenes".format(len(untagged)))
    to_write = {}
    nuevas = 0
    while True:
        chosen_path = untagged[random.randint(0,len(untagged)-1)]
        DROID.view('file://{}'.format(chosen_path), 'image/*')
        input_raw = input("A> ").lower()
        if input_raw == ".":
            break
        elif input_raw in COMANDOS_SKIP:
            print("Ignorando...")
            continue
        """if input("Confirm?") not in COMANDOS_CONFIRMAR:
            continue"""
        tags = input_raw.split()
        INDICE[chosen_path] = tags
        for tag in tags:
            INDICE_INVERTIDO[tag] = INDICE_INVERTIDO.get(tag, []) + [chosen_path]
            #to_write[tag] = to_write.get(tag, []) + [chosen_path]
        untagged.remove(chosen_path)
        nuevas += 1

    with open(SAVE_TAGS_IN, "w") as archivo:
        print("Actualizando base de datos...")
        
        for tag,paths in INDICE_INVERTIDO.items():
            paths_w = " ".join(paths)
            linea = "{},{}\n".format(tag,paths_w)
            archivo.write(linea)
    print("{} ruta(s) taggeada(s)!\n".format(nuevas))



def main():
    
    print("\nPuedes utlizar '.' para salir o 'c' para creditos")
    while True:
        input_raw = input("\nElija modo: F (find) o A (assign)\n> ").lower()
        if input_raw in COMANDOS_FIND:
            modo_find()
        elif input_raw in COMANDOS_ASSIGN:
            modo_assign()
        elif input_raw == ".":
            exit()
        elif input_raw == "c":
            print(CREDITOS)
        else:
            print("Comando no reconocido.")


main()

